<?php
  // Ce n'était pas préciser dans le sujet qu'il fallais absolument utiliser javascript pour lire le fichier
  // On récupère le fichier
  $boardList = file_get_contents('board_list.txt');
  // On sépare chaque lignes
  $boardList = explode("\n", $boardList);
  $compteur = 1;
  // Pour chaques lignes
  foreach ($boardList as $rowOfFile) {
    if ($rowOfFile != ""){
      // On crée une variable qui stoquera le nom de ce plateau de jeu
      $nameOfBoard = "board".$compteur;
      // On crée une variable qui stoquera le plateau de jeu et de nom la variable précédente
      $$nameOfBoard = "[";
      // Pour chaque ligne du plateau de jeu
      $rowOfGame = explode(" / ", $rowOfFile);
      foreach ($rowOfGame as $valueOfRowOfGame) {
        $$nameOfBoard .= "[";
        $valueOfGame = explode("-", $valueOfRowOfGame);
        foreach ($valueOfGame as $value) {
          $$nameOfBoard .= $value.", ";
        }
        $$nameOfBoard = substr($$nameOfBoard, 0, -2)."],";
      }
      $$nameOfBoard = substr($$nameOfBoard, 0, -1)."]";
      $compteur++;
    }
  }
?>
<script  type="text/javascript">
  <?php
    for ($i=1; $i < $compteur; $i++) {
      $nameOfBoard = "board".$i; ?>
      board<?php echo $i; ?>Clean = <?php echo $$nameOfBoard; ?>; <?php
    }
  ?>






  // board1Clean = <?php echo $board1; ?>;
  // board2Clean = <?php echo $board2; ?>;
  // board3Clean = <?php echo $board3; ?>;
</script>


<html>
  <head>
    <title>Hitori Game BONUS - Thomas Dazy</title>
    <script type="text/javascript" src="assets/js/jquery-3.3.1.js"></script>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="assets/css/perso.css">
  </head>
  <body>

    <!-- FOOTER -->
    <div class="footer">
      <button id="newBoard" onclick="generateANewGame()">New Board</button>
      <button id="checkBoard" onclick="checkBoard()">Check</button>
      <button id="resetBoard" onclick="resetBoard()">Reset</button>
      <button id="gameRules" onclick="gameRules()" onmouseover="gameRules()">Game Rules</button>
      <a href="importANewRow.php"><button id="createANewBoard">Importer des plateaux de jeu</button></a>
      <a href="seeboard.php"><button>Voir les plateaux de jeu disponibles</button></a>
      <div id="timer">Timer: 0 secondes</div>
    </div>
    <!-- END FOOTER -->

    <span id="hiddenInputReset" style="display: none;">0</span>
    <span id="boardNumber" style="display: none;">0</span>
    <span id="timerValue" style="display: none;">0</span>

    <!-- CENTERED TABLE -->
    <div style="width:90vw; height: 90vh;">
      <div class="centerOnPage">
        <!-- Tableau de 5x5 -->
        <table id="board">
          <tbody>
            <tr id="rowOne">
              <td class="columnOne">
              </td>
              <td class="columnTwo">
              </td>
              <td class="columnThree">
              </td>
              <td class="columnFour">
              </td>
              <td class="columnFive">
              </td>
            </tr>

            <tr id="rowTwo">
              <td class="columnOne">
              </td>
              <td class="columnTwo">
              </td>
              <td class="columnThree">
              </td>
              <td class="columnFour">
              </td>
              <td class="columnFive">
              </td>
            </tr>

            <tr id="rowThree">
              <td class="columnOne">
              </td>
              <td class="columnTwo">
              </td>
              <td class="columnThree">
              </td>
              <td class="columnFour">
              </td>
              <td class="columnFive">
              </td>
            </tr>

            <tr id="rowFour">
              <td class="columnOne">
              </td>
              <td class="columnTwo">
              </td>
              <td class="columnThree">
              </td>
              <td class="columnFour">
              </td>
              <td class="columnFive">
              </td>
            </tr>

            <tr id='rowFive'>
              <td class="columnOne">
              </td>
              <td class="columnTwo">
              </td>
              <td class="columnThree">
              </td>
              <td class="columnFour">
              </td>
              <td class="columnFive">
              </td>
            </tr>
          </tbody>
        </tables>
        <!-- END CENTERED TABLE -->

      </div>
    </div>
  </body>

  <script type="text/javascript">
    var compteur_php = <?php echo $compteur; ?>;
    // Génération/création du plateau de jeu:
    function newBoard(currentBoard){
      $('#board').removeClass('winner');
      $('.footer').removeClass('winner');
      board = cleanBoard(board);
      boardNumber = getBoardNumber();
      newValue = boardNumber;
      while (newValue == boardNumber) {
        newValue = Math.floor((Math.random() * <?php echo $compteur-1; ?>) + 1);
      }
      switch (newValue) {
        <?php
        for ($i=1; $i < $compteur; $i++) {
          $nameOfBoard = "board".$i; ?>
          case <?php echo $i; ?>:
          board = <?php echo $$nameOfBoard; ?>;
          var boardNumber = <?php echo $i; ?>;
          break;
        <?php } ?>
      }

      $('#boardNumber').html(boardNumber);
      displayBoard(board);
      $('#board td').removeClass('coloredCell');
      return board;
    }

  </script>

  <script type="text/javascript" src="assets/js/perso.js"></script>

</html>
