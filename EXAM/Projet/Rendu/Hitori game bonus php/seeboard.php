<?php
// On récupère le fichier
$boardList = file_get_contents('board_list.txt');
// On sépare chaque lignes
$boardList = explode("\n", $boardList);
$compteur = 1;
// Pour chaques lignes
foreach ($boardList as $rowOfFile) {
  if ($rowOfFile != ""){
    // On crée une variable qui stoquera le nom de ce plateau de jeu
    $nameOfBoard = "board".$compteur;
    // On crée une variable qui stoquera le plateau de jeu et de nom la variable précédente
    $$nameOfBoard = "[";
    // Pour chaque ligne du plateau de jeu
    $rowOfGame = explode(" / ", $rowOfFile);
    foreach ($rowOfGame as $valueOfRowOfGame) {
      $$nameOfBoard .= "[";
      $valueOfGame = explode("-", $valueOfRowOfGame);
      foreach ($valueOfGame as $value) {
        $$nameOfBoard .= $value.", ";
      }
      $$nameOfBoard = substr($$nameOfBoard, 0, -2)."],";
    }
    $$nameOfBoard = substr($$nameOfBoard, 0, -1)."]";
    $compteur++;
  }
}

$action = $_GET['action'];

if ($action == "default")
{
  $board_default_list_content = file_get_contents('board_list_default.txt');
  unlink('board_list.txt');
  $board_list = fopen('board_list.txt', 'x+');
  fwrite($board_list, $board_default_list_content);
}

if ($action == "remove")
{
  $boardList = file_get_contents('board_list.txt');
  // On sépare chaque lignes
  $boardList = explode("\n", $boardList);
  $compteur = 1;
  // Pour chaques lignes
  foreach ($boardList as $rowOfFile) {
    if ($compteur != $_GET['number']) $newBoardList .= $rowOfFile."\n";
    $compteur++;
  }
  unlink('board_list.txt');
  $board_list = fopen('board_list.txt', 'x+');
  fwrite($board_list, $newBoardList);
}

?>


<html>
  <head>
    <title>Hitori Game BONUS - Thomas Dazy</title>
    <script type="text/javascript" src="assets/js/jquery-3.3.1.js"></script>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="assets/css/perso.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>
  <body>


    <div class="footer">
      <button style="display: none;"></button>
      <a href="seeboard.php?action=default"><button>Revenir à la configuration par défaut</button></a>
      <a href="importANewRow.php"><button>Ajouter un plateau de jeu</button></a>
      <a href="index.php"><button>Retour au jeu</button></a>
    </div>


    <div style="width:98vw; height: 98vh;">
      <div class="centerOnPage">
        <table style="width: 98vw;">
          <tr style="width: 98vw;">
        <div>
          <?php
          $boardList = file_get_contents('board_list.txt');
          // On sépare chaque lignes
          $boardList = explode("\n", $boardList);
          $compteur = 1;
          // Pour chaques lignes
          foreach ($boardList as $rowOfFile) {
            if ($rowOfFile != ""){
              if ($compteur == "7" or $compteur == "14") echo "</tr><tr>";
              echo "<td>";
              echo "<table style=\"display: inline-block; margin-left: 10px;\"><tbody>";

              // Pour chaque ligne du plateau de jeu
              $rowOfGame = explode(" / ", $rowOfFile);
              foreach ($rowOfGame as $valueOfRowOfGame) {
                echo "<tr>";
                $valueOfGame = explode("-", $valueOfRowOfGame);
                foreach ($valueOfGame as $value) {
                  echo "<td>".$value."</td>";
                }
                echo "</tr>";
              }
              echo "<tr><td  colspan=\"5\"><a href=\"seeboard.php?action=remove&number=".$compteur."\"><i class=\"remove-icon fas fa-trash-alt\"></i></a></td></tr>";
              echo "</tbody></table>";
              echo "</td>";
              $compteur++;
            }
          }
          ?>
        </tr>
      </table>
        </div>
      </div>
    </div>

  </body>
</html>
