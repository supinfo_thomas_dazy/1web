<?php
$action = $_GET['action'];

if ($action == "import")
{
  $newBoard = "";
  $compteur = 1;
  foreach($_POST as $key => $value) {
    if ($newBoard != ""){
      if ($compteur == 6 or $compteur == 11 or $compteur == 16 or $compteur == 21 or $compteur == 26){
        $newBoard .= " / ";
      }
      else {
        $newBoard .= "-";
      }
    }
    $newBoard .= $value;
    $compteur++;
  }
  $board_list_content = file_get_contents('board_list.txt');
  if (strpos($board_list_content, $newBoard) === false){

    $board_list = fopen('board_list.txt', "w");
    $newBoard = $board_list_content.$newBoard."\n";
    fwrite($board_list, $newBoard);
    fclose($board_list);
    ?>
    <script type="text/javascript">
      alert("Votre plateau de jeu à bien été importé");
    </script>
    <?php
  }
  else
  {
    ?>
      <script type="text/javascript">
        alert("Ce plateau de jeu existe déjà...");
      </script>
    <?php
  }
}
?>
<html>
  <head>
    <title>Hitori Game BONUS - Thomas Dazy</title>
    <script type="text/javascript" src="assets/js/jquery-3.3.1.js"></script>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="assets/css/perso.css">
  </head>
  <body>


    <div class="footer">
      <button type="submit" form="mainForm">Confirmer</button>
      <button type="reset" form="mainForm">Reset</button>
      <a href="seeboard.php"><button>Voir les plateaux de jeu disponibles</button></a>
      <a href="index.php"><button>Retour au jeu</button></a>
    </div>
      <form id="mainForm" action="importANewRow.php?action=import" method="POST">
      <!-- CENTERED TABLE -->
      <div style="width:90vw; height: 90vh;">
        <div class="centerOnPage">
          <!-- Tableau de 5x5 -->
          <table>
            <tbody>
              <tr>
                <td class="square_1">
                  <input type="number" min="1" max="5" value="1" name="square_1">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_2" id="square_2">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_3" id="square_3">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_4" id="square_4">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_5" id="square_5">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_6" id="square_6">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_7" id="square_7">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_8" id="square_8">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_9" id="square_9">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_10" id="square_10">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_11" id="square_11">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_12" id="square_12">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_13" id="square_13">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_14" id="square_14">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_15" id="square_15">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_16" id="square_16">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_17" id="square_17">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_18" id="square_18">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_19" id="square_19">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_20" id="square_20">
                </td>
              </tr>
              <tr>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_21" id="square_21">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_22" id="square_22">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_23" id="square_23">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_24" id="square_24">
                </td>
                <td>
                  <input type="number" min="1" max="5" value="1" name="square_25" id="square_25">
                </td>
              </tr>
            </tbody>
          </tables>
          <!-- END CENTERED TABLE -->



    </form>

    <script type="text/javascript">
      alert("Les plateaux de jeu ajoutés ne pourrons pas être vérifiés... ");
    </script>
  </body>
</html>
