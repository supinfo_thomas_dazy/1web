
//---------------------------------------//
//----------|                 |----------//
//----------| LISTE FONCTIONS |----------//
//----------|                 |----------//
//---------------------------------------//

// Génération/création du plateau de jeu:
function newBoard(currentBoard){
  board = cleanBoard(board);

  boardNumber = getBoardNumber();
  newValue = boardNumber;
  while (newValue == boardNumber) {
    newValue = Math.floor((Math.random() * 3) + 1);
  }


  jQuery.get('board_list.txt', function(data) {
var myvar = data;
alert(data);
});


  switch (newValue) {
    case 1:
      board = [[5, 4, 4, 3, 4], [3, 4, 2, 5, 4], [2, 5, 5, 1, 2], [2, 2, 1, 4, 5], [4, 1, 5, 3, 2]];
      var boardNumber = 1;
      break;
    case 2:
      board = [[4, 4, 4, 2, 2], [4, 2, 3, 5, 1], [1, 1, 2, 5, 4], [1, 3, 4, 1, 5], [3, 5, 4, 4, 1]];
      var boardNumber = 2;
      break;
    case 3:
      board = [[2, 2, 2, 5, 1], [5, 4, 2, 3, 4], [5, 1, 4, 5, 2], [2, 4, 5, 1, 3], [3, 5, 1, 1, 4]];
      var boardNumber = 3;
      break;
  }

  $('#boardNumber').html(boardNumber);
  displayBoard(board);
  $('#board td').removeClass('coloredCell');
  return board;
}

function getBoardNumber() {
    var boardNumber = $('#boardNumber').html();
    return boardNumber;
}

function changeRowName(value) {
  switch (value) {
    case 0:
      return "rowOne";
      break;
    case 1:
      return "rowTwo";
      break;
    case 2:
      return "rowThree";
      break;
    case 3:
      return "rowFour";
      break;
    case 4:
      return "rowFive";
      break;
  }
}

function changeColumnName(value) {
  switch (value) {
    case 0:
      return "columnOne";
      break;
    case 1:
      return "columnTwo";
      break;
    case 2:
      return "columnThree";
      break;
    case 3:
      return "columnFour";
      break;
    case 4:
      return "columnFive";
      break;
  }
}


// On efface le plateau de jeu
function cleanBoard(board) {
  for (var i = 0; i < board.length; i++) {
    for (var j = 0; j < board.length; j++) {
      var currentRow = changeRowName(i);
      var currentColumn = changeColumnName(j);
      $('#' + currentRow + ' > .' + currentColumn).html("");

    }
  }
  board = [];
  return board;
}

// Affichage du plateau de jeu
function displayBoard(board) {
  for (var i = 0; i < board.length; i++) {
    for (var j = 0; j < board.length; j++) {
      var currentRow = changeRowName(i);
      var currentColumn = changeColumnName(j);
      $('#' + currentRow + ' .' + currentColumn).html(board[i][j]);
    }
  }
}

// Quand on clique sur le button 'New Game'
function generateANewGame(elapsed, start, boardNumber) {
  if (confirm('Êtes vous sûr ?\nCeci réinitialisera le timer.')) {
        board = newBoard(board);
        $('#hiddenInputReset').val("1");
    }
}

// Quand on clique sur le bouton (ou passe la souris au dessus) Game Rules qui permet d'afficher les règles du jeu
function gameRules() {
  alert("In a Hitori puzzle you start with a full grid and must eliminate digits (or letters in some puzzles) by shading them in so that no digit occurs more than once in any row or column. Unlike Sudoku, there is no requirement to have every digit in each row or column - this would not be possible in any case.\nAdditionally, the finished grid must be a valid \"Japanese crossword\". What this means in practice is that all of the unshaded squares must be connected into one single region, just as they are in a traditional British or US crossword, but also that no shaded square can touch any other shaded square (except diagonally).\n\nHitori exampleThe rules of Hitori, then, can be summarised as:\n\n• Shade some squares so that no unshaded digit or letter is repeated in a row or column.\n• No shaded square can touch any other shaded square horizontally or vertically (but they may touch diagonally).\n• You must be able to 'travel' from any unshaded square to any other unshaded square simply by moving left/right/up/down from unshaded square to unshaded square.\n• Every Hitori puzzle only ever has one possible solution, and it can always be reached via reasonable logical deduction. In other words, guessing is never required.\n\n\nSource: https://www.puzzlemix.com/Hitori");
}

$('td').click(function() {
  var pattern = /coloredCell/;
  var classListOfCell = this.classList;
  var colored = pattern.test(classListOfCell);
  if (colored) { $(this).removeClass('coloredCell'); }
  else { $(this).addClass('coloredCell'); }
});

// Reset le plateau de jeu (décolore toutes les cases colorées)
function resetBoard(board) {
  if (confirm('Êtes vous sûr ?')) {
    $('#board td').removeClass('coloredCell');
  }
}

// Fonction qui vérifie le plateau
function checkBoard(board) {
  var boardNumber = getBoardNumber();
  winningBoard = [];
  var hasWon = 1;
  var finishedLoop = 0;

  switch (parseInt(boardNumber)) {
    case 1:
      winningBoard = [[0, 0, 1, 0, 1], [0, 1, 0, 0, 0], [0, 0, 1, 0, 1], [1, 0, 0, 0, 0], [0, 0, 0, 1, 0]];
      break;
    case 2:
      winningBoard = [[1, 0, 1, 0, 1], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0], [1, 0, 0, 0, 0], [0, 0, 1, 0, 1]];
      break;
    case 3:
      winningBoard = [[1, 0, 1, 0, 0], [0, 0, 0, 0, 1], [1, 0, 0, 1, 0], [0, 1, 0, 0, 0], [0, 0, 0, 1, 0]];
      break;
  }

  while (hasWon == 1 && finishedLoop == 0) {
    for (var i = 0; i < 5; i++) {
      for (var j = 0; j < 5; j++) {
        var currentRow = changeRowName(i);
        var currentColumn = changeColumnName(j);

        var colored = $('#' + currentRow + ' .' + currentColumn).hasClass("coloredCell");
        if (colored) {
          var coloredValue = 1;
        }
        else {
          var coloredValue = 0;
        }
        if (winningBoard[i][j] != coloredValue)
        {
          hasWon = 0;
        }
      }
    }
    finishedLoop = 1;
  }

  // On récupère la valeur actuelle du timer
  timerValue = $('#timerValue').html();

  if (hasWon == 1) {
    alert("You won!\nAnd you took: " + timerValue + " seconds !");
    $('#board').addClass('winner');
    $('.footer').addClass('winner');
  }
  else {
    alert("You didn't won!");
    $('#hiddenInputReset').val("2");
  }
}


//--------------------------------------//
//-------|                      |-------//
//-------| STRUCTURE PRINCIPALE |-------//
//-------|                      |-------//
//--------------------------------------//


// Message d'accueil
$( document ).ready(function() {
    alert("Projet Hitori Game par Thomas Dazy - 294828.");
});

// Initialisation des variables
var currentScore = 0;
boardNumber = 0;
board = newBoard(board);
timer = 0;
var isWinner = 0;
$('#hiddenInputReset').val("0");
var start = +new Date();

// Quand on génère un nouveau jeu, on reset le Timer
$("#newBoard").click((start = new Date()))

// Fonction qui update le timer
window.setInterval(function() {
  var hiddenInputReset = $('#hiddenInputReset').val();
  // alert(hiddenInputReset);

  var elapsed = +new Date() - start;

  // On vérifie qu'il y a bien une seconde qui s'est écoulé
  if ((new Date() - start) >= 1000)
  {
    $('#timer').html("Timer: " + Math.round(elapsed/1000) + " secondes");
    $('#timerValue').html(Math.round(elapsed/1000));
  }

  if (hiddenInputReset == "1")
  {
    elapsed = 0;
    start = new Date();
    $('#hiddenInputReset').val("0");
  }
  if (hiddenInputReset == "2")
  {
    start = start - 15000;
    $('#hiddenInputReset').val("0");
  }
}, 500);
